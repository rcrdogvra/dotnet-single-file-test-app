Single File Hello World App

This is a single file hello world application that should
generate a framework independet self-contained executable

```
dotnet publish
```

# Run the program

```
bin/Debug/netcoreapp3.0/win-x64/publish/
```

